import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int numberofelement = 0;
        int uniquenumberofelement = 0;
        int arr[];
        int uniquearr[];
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element:");
        numberofelement = sc.nextInt();
        arr = new int[numberofelement];
        uniquearr = new int[numberofelement];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element"+ i +":");
            arr[i] = sc.nextInt();
            int index = -1;
            for (int j = 0; j < uniquenumberofelement; j++) {
                if(uniquearr[j] == arr[i]) {
                    index = j;
                }
            }
            if(index<0) {
                uniquearr[uniquenumberofelement] = arr[i];
                uniquenumberofelement++;
            }
        }
        System.out.print("All number:");
        for (int i = 0; i < uniquenumberofelement; i++) {
            System.out.print(uniquearr[i]+" ");
        }
    }
}
