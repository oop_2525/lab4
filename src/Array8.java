import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        int sum = 0;
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for(int i=0;i<arr.length;i++) {
            System.out.print("Please input arr["+ i + "]:");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for(int i=0;i<arr.length;i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        for(int i=0;i<arr.length;i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);
        double avg = ((double)sum)/arr.length;
        System.out.println("avg = "+ avg);
        
        int minindex = 0;
        for(int i=1;i<arr.length;i++) {
            if(arr[minindex]>arr[i]) {
                minindex = i;
            }
        }
        System.out.println("min = "+ arr[minindex]);

        int maxindex = 0;
        for(int i=1;i<arr.length;i++) {
            if(arr[maxindex]<arr[i]) {
                maxindex = i;
            }
        }
        System.out.println("max = "+ arr[maxindex]);
    }
}
